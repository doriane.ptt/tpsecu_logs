/*
 * server side
*/

// include module for nodejs
var express = require('express');
var app = express();
const fs = require('fs');
var lineReader = require('line-reader');
const readLine = require('readline');

var path = "./log/access.log";

let network;

app.use(express.static('.'))

var ips = new Array();
var infos = new Array();
/*
 * function that will read the file access.log and store ips and infos in array
*/
function readfile_access(){
    lineReader.eachLine(path, function(line, last) {
        // add ips and infos in array to store it locally.
        ips.push(line.split(' - - ')[0]);
        infos.push(line.split(' - - ')[1]);
        if (last) {
            // or check if it's the last one.
        }
    });
}



/* just send HTTP 200 code when /api/status is called */
app.get('/api/status', function(req, res) {
    res.sendStatus(200);
});

/* compute parameter1 * parameter2 and return the result in json format i

 * @param {int} parameter1 - the first member to multiply
 * @param {int} parameter2 - the second member to multiply
 */
app.get('/api/ips', function(req, res) {
  readfile_access();
  ret = {};
  ret['result'] = ips;
  res.json(ret);
});

/* listen on the first port available 
 *  then open the browser to this url
 */
const portfinder = require('portfinder');
portfinder.getPortPromise()
  .then((portFound) => {
    let server = app.listen(portFound, function() {
        let host = server.address().address
        port = server.address().port
        console.log("app listening at: http://%s:%s", host, port)
        //require("openurl").open("http://localhost:" + port);
    })
  })
  .catch((err) => {
    console.log("can't find an available port within: [" + portfinder.basePort + ';' + portfinder.highestPort + ']');
  });
