import { isIP } from "net";

/*
 * @TO-DO list :
 *  Utilisation des adresses IP pour afficher sur la map.
 *  Vérification que l'IP soit unique
 *  Éviter les correspondance lat/lon si même adresse IP.
 *  Garder une partie des calculs dans le cache
 *  Sérialisation d'une partie des adresses
 *  Affichage du type de requête, url et version du navigateur client
*/

var mymap = L.map('mapid').setView([46.20222, 6.14569], 13);

// footer of the map
L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZG90aGkiLCJhIjoiY2s1emY4c2hjMGs5ZjNkcDNkdjZmdjM0bCJ9.In1XDSZxJkf_vyQro_gIrQ', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    accessToken: 'your.mapbox.access.token'
}).addTo(mymap);

var marker = L.marker([46.20222, 6.14569]).addTo(mymap);

marker.bindPopup("Howdy, I'm a popup! :D").openPopup();


function onMapClick(e) {
    popup
        .setLatLng(e.latlng)
        .setContent("You clicked the map at " + e.latlng.toString())
        .openOn(mymap);
}

mymap.on('click', onMapClick);

//#region 

/*
 * Calculate distance between two given points
 * @param {number} lat1 - latitude of the first point
 * @param {number} lon1 - longitude of the first point
 * @param {number} lat2 - latitude of the second point
 * @param {number} lon2 - longitude of the second point
*/
function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
    const EarthRadius = 6371;   // Radius of the earth in km.
    let dLat = deg2rad(lat2 - lat1);  //deg2rad below.
    let dLon = deg2rad(lon2 - lon1);
    let a = Math.sin(dLat / 2.0) * Math.sin(dLat / 2.0) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
        Math.sin(dLon / 2.0) * Math.sin(dLon / 2.0);
    let c = 2.0 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    let d = EarthRadius * c;  // distance in km.
    return d;
  }
  
  /*
   * Convert degree to radian
   * @param {number} deg - angle in degree
  */
  function deg2rad(deg){
    return deg * (Math.PI/180);
  }
  
  /*
   * Tranform JSON into array
   * @param {JSON data} json - JSON data
  */
  function json2array(json){
    let result = [];
    let keys = Object.keys(json);
    keys.forEach(function(key){
      result.push(json[key]);
    });
    return result;
  }
  
  
//#endregion

function ipLookUp(ip) {
    // using fetch rather than ajax to avoid override
    fetch('http://ip-api.com/json/' + ip).then(
        function success(resp) {
            return resp.json();
        }).then(function(data) {
            // @TO-DO create a marker rather than create a new map centering on location
            createImageBitmap(data['lat'])
        })
}


function GetIPs() {
   fetch('api/ips')
        .then(function(response) {
          return response.json();
        })
        .then(function(myJson) {
            let display = '';
            myJson['result'].forEach(element => {
                ipLookUp(element);
            });
          document.getElementById("result").innerHTML = " Result=" + myJson['result'];
      });
}